import { s__ } from '~/locale';

export const USAGE_BY_MONTH_HEADER = s__('UsageQuota|Usage by month');
export const USAGE_BY_PROJECT_HEADER = s__('UsageQuota|Usage by project');

export const PIPELINES_TAB_METADATA_EL_SELECTOR = '#js-pipeline-usage-app';
